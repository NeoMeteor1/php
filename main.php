<?php

require_once(dirname(__FILE__).'/constants.php');

$array = get_defined_constants(true)['user'];

$result = $array['Two'] * $array['Three'];

$sequels = "sequels";
$trilogy = "trilogy";
$films = "films";
$star = "Star";
$released = "released";

$wiki_sw =
    <<<STRING

    The film series began with $star Wars, $released on May 25, 1977. This was followed by two $sequels: The Empire Strikes Back, $released on May 21, 1980, and Return of the Jedi, $released on May 25, 1983. The opening crawl of the $sequels disclosed that they were numbered as "Episode V" and "Episode VI" respectively, though the $films were generally advertised solely under their subtitles. Though the first film in the series was simply titled $star Wars, with its 1981 re-release it had the subtitle Episode IV: A New Hope added to remain consistent with its sequel, and to establish it as the middle chapter of a continuing saga.

    In 1997, to correspond with the 20th anniversary of the original film, Lucas $released a "Special Edition" of the $star Wars $trilogy to theaters. The re-release featured alterations to the three $films, primarily motivated by the improvement of CGI and other special effects technologies, which allowed visuals that were not possible to achieve at the time of the original filmmaking. Lucas continued to make changes to the $films for subsequent releases, such as the first ever DVD release of the original $trilogy on September 21, 2004, and the first ever Blu-ray release of all six $films on September 16, 2011. Reception of the Special Edition was mixed,prompting petitions and fan edits to produce restored copies of the original $trilogy.

    More than two decades after the release of the original film, the series continued with a prequel $trilogy; consisting of Episode I: The Phantom Menace, $released on May 19, 1999; Episode II: Attack of the Clones, $released on May 16, 2002; and Episode III: Revenge of the Sith, $released on May 19, 2005. On August 15, 2008, $star Wars: The Clone Wars was $released theatrically as a lead-in to the animated TV series of the same name. $star Wars: The Force Awakens was $released on December 18, 2015.

STRING;

echo "The result of " . $array['Two'] . " * " . $array['Three'] . " = " . $result;

echo $wiki_sw;
