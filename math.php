<?php

class Math
{
    private $opCheck = true;

    //Adds the provided numbers, or displays message requesting more numbers
    public function add()
    {
        $makeArray = func_get_args();

        //This will extract the data/array from it's location if the values were provided by $opCheck
        if (is_array($makeArray[0])) {
            $makeArray = array_splice($makeArray[0],0);
        }

        //Performs a check for the desired operator if this hasn't been done already.
        if ($this->opCheck == true) {
            $this->doOperation($makeArray);
        }

        //Checks the number of provided integers and adds them if there are at least two available.
        switch ($makeArray) {
            case ($makeArray >= 2):

                $finalnum = array_sum($makeArray);

                echo "The sum of these numbers is $finalnum <br>";
                break;

            default:
                echo "You have to use more numbers if you want to use this function";
        }
    }

    //Subtracts the provided numbers, or displays message requesting more numbers
    public function subtract()
    {
        $finalnum = 0;
        $firstnum = 0;
        $makeArray = func_get_args();

        //This will extract the data/array from it's location if the values were provided by $opCheck
        if (is_array($makeArray[0])) {
            $makeArray = array_splice($makeArray[0],0);
        }

        //Performs a check for the desired operator if this hasn't been done already.
        if ($this->opCheck == true) {
            $this->doOperation($makeArray);
        }

        //Checks the number of provided integers and subtracts them if there are at least two available.
        switch($makeArray) {
            case ($makeArray >= 2):

                foreach ($makeArray as $currentnum) {

                    if ($firstnum == 0) {
                        $finalnum = $currentnum;
                        $firstnum = 1;
                        continue;
                    }

                    $finalnum -= $currentnum;
                }

                echo "The difference of these numbers is $finalnum <br>";
                break;

            default:
                echo "You have to use more numbers if you want to use this function";

        }
    }

    //Multiplies the provided numbers, or displays message requesting more numbers
    public function multiply()
    {
        $makeArray = func_get_args();

        //This will extract the data/array from it's location if the values were provided by $opCheck
        if (is_array($makeArray[0])) {
            $makeArray = array_splice($makeArray[0],0);
        }

        //Performs a check for the desired operator if this hasn't been done already.
        if ($this->opCheck == true) {
            $this->doOperation($makeArray);
        }

        //Checks the number of provided integers and multiplies them if there are at least two available.
        switch($makeArray) {
            case ($makeArray >= 2):

                $finalnum = array_product($makeArray);

                echo "The product of these numbers is $finalnum <br>";
                break;

            default:
                echo "You have to use more numbers if you want to use this function";

        }
    }

    //Divides the provided numbers, or displays message requesting more numbers
    public function divide()
    {
        $makeArray = func_get_args();

        //This will extract the data/array from it's location if the values were provided by $opCheck
        if (is_array($makeArray[0])) {
            $makeArray = array_splice($makeArray[0],0);
        }

        //Performs a check for the desired operator if this hasn't been done already.
        if ($this->opCheck == true) {
            $this->doOperation($makeArray);
        }

        //Checks the number of provided integers and divides them if there are at least two available.
        switch($makeArray) {
            case ($makeArray >= 2):

                $finalnum = 0;
                $firstnum = 0;

                foreach($makeArray as $currentnum) {

                    if ($firstnum == 0) {
                        $finalnum = $currentnum;
                        $firstnum = 1;
                        continue;
                    }

                    $finalnum = $finalnum / $currentnum;
                }

                echo "The quotient of these numbers is $finalnum <br>";
                break;

            default:
                echo "You have to use more numbers if you want to use this function <br>";
        }
    }
    private function doOperation ($num_args)
    {
        $this->opCheck=false;

        // If the first element of provided array is non-numeric,
        // it could be the desired operator and should be extracted.
        if (!is_numeric($num_args[0])) {
            $operator = $num_args[0];
            array_splice($num_args, 0, 1);

            switch ($operator) {
                case "+":
                    $this->add($num_args);
                    exit;
                case "-":
                    $this->subtract($num_args);
                    exit;
                case "*":
                    $this->multiply($num_args);
                    exit;
                case "/":
                    $this->divide($num_args);
                    exit;
                default:
                    echo "Invalid character in list <br>";
                    exit;
            }

        }

    }

}

$equation = new Math;
$equation->multiply(2, 4, 6, 8);