<?php

require_once('math.php');

$numberMachine = new Math();

//Adding numbers
$numberMachine->add (1, 2, 3, 4);
$numberMachine->add (2, 4, 6, 8);
$numberMachine->add (3, 6, 9, 12);

//Subtracting numbers
$numberMachine->subtract (1, 2, 3, 4);
$numberMachine->subtract (2, 4, 6, 8);
$numberMachine->subtract (3, 6, 9, 12);


//Multiplying numbers
$numberMachine->multiply (1, 2, 3, 4);
$numberMachine->multiply (2, 4, 6, 8);
$numberMachine->multiply (3, 6, 9, 12);


//Dividing numbers
$numberMachine->divide (1, 2, 3, 4);
$numberMachine->divide (2, 4, 6, 8);
$numberMachine->divide (3, 6, 9, 12);



