<?php

//numeric array from 0 - 100 in intervals of 3
$numbers = range(0,100,3);

//Counter for the upcoming "do-while" loop
$x = 0;

//The first number in the upcoming for "do-while" would always be "first",
//so there's no need for complicated math to figure that out
echo "First Number " . "<br>";

//for loop to display:
//1. Name of numbers in intervals of 1/3rd $value for 3, 9, and 15
//2. A message on numbers that are multiples of 7 or 10
do {

    switch ($numbers[$x]) {

        case 3:
            echo "Three " . "<br>";
            continue;

        case $numbers[$x] % 7 === 0:
            echo "Sevens are lucky, this number has " . ($numbers[$x] / 7) . "<br>";
            continue;

        case 9:
            echo str_repeat("Nine ", 3) . "<br>";
            continue;

        case $numbers[$x] % 10 === 0:
            echo $numbers[$x] . " is a round number " . "<br>";
            continue;

        case 15:
            echo str_repeat("Fifteen ", 5) . "<br>";
            continue;

    }
    $x++;
    
}while($x < count($numbers));

//The last number in the previous "do-while" loop would always be "last",
//so there's no need for complicated math to figure that out.
echo "Last Number " . "<br>";
