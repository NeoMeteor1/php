<?php

$a = 9;
$b = 9;


//With $a = 9 and $b = 9, the result should be 18
function add ($firstnum, $secondnum)
{
    $result = $firstnum + $secondnum;
    print $result . "<br>";
}

//With $a = 9 and $b = 9, the result should be 0
function subtract ($firstnum, $secondnum)
{
    $result = $firstnum - $secondnum;
    print $result . "<br>";
}

//With $a = 9 and $b = 9, the result should be 81
function multiply (&$firstnum, $secondnum)
{
    $firstnum = $firstnum * $secondnum;
    print $firstnum . "<br>";
}

//With $a = 81 (if "multiply" was used first) and $b = 9, the result should be 9 (or 1 if "multiply" was NOT used first)
function divide (&$firstnum, $secondnum)
{
    $firstnum = $firstnum / $secondnum;
    print $firstnum . "<br>";
}

//Function that compares two variables, with the option to also compare the variables' type.
function compare ($firstcomp, $secondcomp, $tf = false)
{
    switch ($tf) {
        //The two variables are equal (type ignored/juggled)
        case false && ($firstcomp == $secondcomp):
            print $firstcomp . " and " . $secondcomp . " are equal. <br>";
            break;

        //The two variables are equal and of the same type
        case true && ($firstcomp === $secondcomp):
            print $firstcomp . " and " . $secondcomp . " are equal, and they are both of the same type. <br>";
            break;

        //The two variables are equal, but not the same type
        case true && ($firstcomp == $secondcomp) && ($firstcomp !== $secondcomp):
            print $firstcomp . " and " . $secondcomp . " are equal, but they are not of the same type. <br>";
            break;

        //The two variables are not equal
        default:
            print $firstcomp . " and " . $secondcomp . " are not equal. <br>";
            break;

    }


}

add($a,$b);
subtract($a,$b);
multiply($a,$b);
divide($a,$b);

compare(4, "4");
compare( 5, "5", true);
compare(4, 4.0); //Assuming no bccomp()
compare(5., 5.0, true); //Assuming no bccomp()
